<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    use HasFactory;

    protected $table = 'education';
    protected $fillable = [
        'degree',
        'institution',
        'institution_link',
        'start_date',
        'end_date',
        'still_studying',
    ];
}
