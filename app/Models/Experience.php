<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    use HasFactory;

    protected $table = 'experiences';
    protected $fillable = [
        'job_title',
        'company',
        'company_link',
        'start_date',
        'end_date',
        'still_working',
    ];
}
