<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    use HasFactory;

    protected $table = 'websites';

    protected $fillable = [
        'favicon',
        'og_title',
        'og_image',
        'og_url',
        'og_type',
        'og_description',
        'og_locale',
        'og_sitename',
        'logo',
        'footer',
    ];

    protected $appends = ['path_logo', 'path_favicon', 'path_og_image'];
}
