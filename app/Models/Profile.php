<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $table = 'profiles';
    protected $fillable = [
        'fullname',
        'nickname',
        'birthday',
        'address',
        'profession',
        'cv',
        'email',
        'photo',
        'about',
        'facebook',
        'instagram',
        'twiter',
        'linkedin',
        'youtube',
        'gitlab',
        'phone',
        'whatsapp',
        'telegram',
    ];

    protected $appends = ['path_cv', 'path_photo'];
}
